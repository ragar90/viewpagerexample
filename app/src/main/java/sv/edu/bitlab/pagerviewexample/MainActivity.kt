package sv.edu.bitlab.pagerviewexample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val pager = findViewById<ViewPager>(R.id.view_pager)
        val tabLayout = findViewById<TabLayout>(R.id.tabs)
        val pagerAdapter = ExamplePagerAdapter(supportFragmentManager)
        pager.adapter = pagerAdapter
        pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(state: Int) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onPageSelected(position: Int) {
                cambiarColorChibolita(position)
            }

        })
        tabLayout.setupWithViewPager(pager)
    }

    fun cambiarColorChibolita(position: Int) {
        var chibolitas = arrayListOf(1, 2, 3)
        val total = chibolitas
            .map { ch -> ch *3 }
            .filter { n -> n % 2 == 0 }
            .reduce{ acumulador, numero -> acumulador + numero}
        //chibolitas[position] = true
    }
}
