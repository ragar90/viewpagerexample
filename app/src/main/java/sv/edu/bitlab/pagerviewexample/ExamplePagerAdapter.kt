package sv.edu.bitlab.pagerviewexample

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.FragmentStatePagerAdapter

class ExamplePagerAdapter(manager: FragmentManager ) : FragmentPagerAdapter(manager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    override fun getItem(position: Int): Fragment {
        return when(position){
            0 -> TabAFragment.newInstance()
            1 -> TabBFragment.newInstance()
            else -> throw RuntimeException("Position not supported")
        }
    }

    override fun getCount(): Int {
        return TOTAL_PAGES
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when(position){
            0 -> "TAB A"
            1 -> "TAB B"
            else -> throw RuntimeException("Position not supported")
        }
    }

    companion object{
        const val TOTAL_PAGES = 2
    }
}